package mike.migotest1

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import mike.migotest1.repo.Repository
import mike.migotest1.repo.StatusModel

class MainViewModel(app: Application) : AndroidViewModel(app) {

    val publicStatus by lazy { MutableLiveData<StatusModel>() }
    val privateStatus by lazy { MutableLiveData<StatusModel>() }

    val publicStatusError by lazy { MutableLiveData<String>() }
    val privateStatusError by lazy { MutableLiveData<String>() }

    fun fetchStatus(){
        viewModelScope.launch(Dispatchers.IO){
            Repository.getPublicStatus().observe({
                    publicStatus.postValue(it)
                },{
                    publicStatusError.postValue("Error : ${it.message}\nError code: ${it.status}")
                }
            )

            Repository.getPrivateStatus().observe({
                    privateStatus.postValue(it)
                },{
                    privateStatusError.postValue("Error : ${it.message}\nError code: ${it.status}")
                }
            )
        }
    }
}
package mike.migotest1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel.publicStatus.observe(this, Observer {
            tv_public_status.text = """
                Result: Success
                Status : ${it.status}
                Message : ${it.message}
            """.trimIndent()
        })

        viewModel.publicStatusError.observe(this, Observer {
            tv_public_status.text = """
                Result: Error
                $it
            """.trimIndent()
        })

        viewModel.privateStatus.observe(this, Observer {
            tv_private_status.text = """
                Result: Success
                Status : ${it.status}
                Message : ${it.message}
            """.trimIndent()
        })

        viewModel.privateStatusError.observe(this, Observer {
            tv_private_status.text = """
                Result: Error
                $it
            """.trimIndent()
        })
    }

    fun onButtonClick(view: View) {
        tv_public_status.text = "Processing..."
        tv_private_status.text = "Processing..."
        viewModel.fetchStatus()
    }
}
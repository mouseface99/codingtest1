package mike.migotest1.repo

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Repository {

    private val publicRetrofit = Retrofit.Builder()
        .baseUrl("https://code-test.migoinc-dev.com")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private val privateRetrofit = Retrofit.Builder()
        .baseUrl("http://192.168.2.2")
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val publicService = publicRetrofit.create(APIService::class.java)
    val privateService = privateRetrofit.create(APIService::class.java)


    fun getPublicStatus(): RepoResponse<StatusModel>{
        return safeApiCall { publicService.getStatus() }
    }

    fun getPrivateStatus(): RepoResponse<StatusModel>{
        return safeApiCall { privateService.getStatus() }
    }

    private fun <MODEL : Any> safeApiCall(
        call: () -> Call<MODEL>
    ): RepoResponse<MODEL> {
        try {
            val response = call.invoke().execute()
            val result =  if (response.isSuccessful){
                Result.Success(response.body()!!)
            } else {
                Result.Fail(response.code())
            }

            var data: MODEL? = null
            var errorObj: RepoError? = null

            when (result) {
                is Result.Success -> {
                    data = result.data
                }
                is Result.Fail -> {
                    errorObj =
                        RepoError(result.errorCode, "Server error, code: ${result.errorCode}")
                }
            }

            return RepoResponse(data, errorObj)

        } catch (e: java.lang.Exception) {
            return RepoResponse(null, RepoError(-1, "System error\n$e"))
        }
    }
}

data class RepoResponse<T>(
    val data: T?,
    val errObj: RepoError? = null
) {
    fun observe(success: (T) -> Unit, error: ((RepoError) -> Unit)? = null): RepoResponse<T> {
        if (data != null)
            success.invoke(data)
        else if (error != null && errObj != null)
            error.invoke(errObj)

        return this
    }
}

data class RepoError(
    val status: Int,
    val message: String
)

sealed class Result<out T : Any> {
    data class Success<out T : Any>(val data: T) : Result<T>()
    data class Fail(val errorCode: Int) : Result<Nothing>()
}
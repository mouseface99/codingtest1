package mike.migotest1.repo

import retrofit2.Call
import retrofit2.http.GET

interface APIService {

    @GET("/status")
    fun getStatus(): Call<StatusModel>
}
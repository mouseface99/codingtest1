package mike.migotest1.repo

data class StatusModel(
    var status: Int,
    var message: String
)